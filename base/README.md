Base ArgoCD Configuration
=========================

Deployment of ArgoCD, common to all clusters:

 - Upstream ArgoCD definitions
 - set pull policy to IfNotPresent
 - namespace
 - basic ArgoCD projects

