Configuration of Test Cluster Instance
======================================

Test cluster specific configuration for

 - role based access
 - ArgoCD service node ports
 - ingress

and ArgoCD apps which read ArgoCD definitions in git repositories for

 - system components
 - training apps
 - Jupyter Hub
