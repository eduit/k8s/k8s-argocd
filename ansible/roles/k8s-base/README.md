Cluster Initialization
======================

If running a Rancher controlled k8s cluster (native or imported), namespaces have to be assigned to Rancher projects to be visible. This happens outside of k8s and cannot be managed with declarations.

For ArgoCD, this role creates the namespace *argocd* and assigns it to the *System* project, with the help of the Rancher CLI executable. It can be installed locally with the *k8s-controller* role.

You first have to create an access token and perform a `rancher login` to enable access by the CLI.