Ansible Playbooks
===

Tools Installation
---

Install/update some tools with this playbook run:

```
sudo ansible-playbook -l localhost pb_controlhost.yml
```

Binaries will be installed in `/usr/local/bin`, or set the directory with

```
ansible-playbook -e "k8s_install_basedir=$HOME/bin k8s_tarball_cache=$HOME/.cache/k8s_tarball -l localhost pb_controlhost.yml
```


Tools installed:

 - kubectl
 - argocd CLI
 - kubeseal
 - rancher CLI
 - kustomize


Basic Cluster Elements
---

Some components in the k8s clusters have to be configured imperatively, ie. Rancher projects cannot be configured declaratively.
Install required ansible collections first:

```
ansible-galaxy collection install -r roles/requirements.yml
```

Run the playbook

```
ansible-playbook -l localhost pb_k8s.yml
```

to create the projects defined in `host_vars/localhost.yml`
